#pragma once

#include "BottomTagger.hh"
#include "Reader.hh"

namespace analysis {

class Doublet;

/**
 * @brief Semi leptonic heavy higgs BDT tagger
 *
 */
class BosonTagger : public BranchTagger<PairBranch> {

public:

    BosonTagger();

    int Train(const Event &event, const PreCuts &pre_cuts, Tag tag) const final;

    std::vector<Doublet> Multiplets(const Event& event, const PreCuts& pre_cuts, const TMVA::Reader& reader) const;

    int GetBdt(const Event &event, const PreCuts &pre_cuts, const TMVA::Reader &reader) const final;

    std::string Name() const final { return "Boson"; }

protected:

private:

    bool Problematic(const Doublet& doublet, const PreCuts& pre_cuts, Tag tag) const;

    bool Problematic(const Doublet& doublet, const PreCuts& pre_cuts) const;

    Reader<BottomTagger> bottom_reader_;

    float boson_mass_window = 80;

};

}
