#ifdef __MAKECINT__

// #pragma link off all globals;
// #pragma link off all classes;
// #pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ defined_in "Branches.hh";

// #pragma link C++ function analysis::Red;
// #pragma link C++ function analysis::Blue;

#endif
