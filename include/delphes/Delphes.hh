#pragma once

#include "classes/DelphesClasses.h"

namespace delphes {

typedef ::Event Event;
typedef ::LHCOEvent LHCOEvent;
typedef ::LHEFEvent LHEFEvent;
typedef ::HepMCEvent HepMCEvent;
typedef ::GenParticle GenParticle;
typedef ::Vertex Vertex;
typedef ::MissingET MissingET;
typedef ::ScalarHT ScalarHT;
typedef ::Rho Rho;
typedef ::Weight Weight;
typedef ::Photon Photon;
typedef ::Electron Electron;
typedef ::Muon Muon;
typedef ::Jet Jet;
typedef ::Track Track;
typedef ::Tower Tower;
typedef ::HectorHit HectorHit;
typedef ::Candidate Candidate;

}
