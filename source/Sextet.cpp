#include "Sextet.hh"

namespace analysis {

const Triplet& Sextet::Triplet1() const
{
    return Multiplet1();
}

const Triplet& Sextet::Triplet2() const
{
    return Multiplet2();
}

}
